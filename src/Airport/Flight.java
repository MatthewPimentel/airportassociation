/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Airport;
import java.util.Date;
/**
 *
 * @author Matthew
 */
public class Flight {
    private String number;
    private Date date;
    
    public String getNumber(){
        return number;
    }
    
    public void setNumber(String number){
        this.number = number;
    }
    
    public Date date(){
        return date;
    }
    
    public void date(Date date){
        this.date = date;
    }
}
